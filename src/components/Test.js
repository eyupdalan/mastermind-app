import React, {Component} from 'react';
import {connect} from "react-redux";
import {bindActionCreators} from "redux";
import * as testActions from "../redux/actions/testAction";

export class Test extends Component {

    componentDidMount() {
        this
            .props
            .actions
            .testAction("test");
    }

    render() {
        return (
            <div>Test
                <span>{this.props.testReducer}</span>
            </div>
        );
    }
}

const mapStateToProps = state => ({testReducer: state.testReducer});

const mapDispatchToProps = dispatch => ({
    actions: bindActionCreators({
        ...testActions
    }, dispatch)
});

export default connect(mapStateToProps, mapDispatchToProps)(Test);