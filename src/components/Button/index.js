import React, {Component} from 'react';
import PropTypes from "prop-types";
import {Button, Dropdown, Menu} from 'antd';
import {connect} from "react-redux";
import {bindActionCreators} from "redux";
import * as currentItemActions from "../../redux/actions/currentItemActions";

export class ItemButton extends Component {
    static propTypes = {
        buttonIndex: PropTypes.number
    }

    state = {
        buttonIndex: -1
    }

    onClickMenuButton = (className) => {
        return () => this
            .props
            .actions
            .setCurrentButtonColor(this.props.buttonIndex, className);
    }

    render() {
        const buttonMenu = <Menu>
            <Menu.Item>
                <Button
                    shape="circle"
                    className="yellow"
                    onClick={this.onClickMenuButton("yellow")}/>
            </Menu.Item>
            <Menu.Item>
                <Button shape="circle" className="red" onClick={this.onClickMenuButton("red")}/>
            </Menu.Item>
            <Menu.Item>
                <Button
                    shape="circle"
                    className="blue"
                    onClick={this.onClickMenuButton("blue")}/>
            </Menu.Item>
            <Menu.Item>
                <Button
                    shape="circle"
                    className="orange"
                    onClick={this.onClickMenuButton("orange")}/>
            </Menu.Item>
            <Menu.Item>
                <Button
                    shape="circle"
                    className="purple"
                    onClick={this.onClickMenuButton("purple")}/>
            </Menu.Item>
        </Menu>;

        return (
            <div>
                <Dropdown overlay={buttonMenu} trigger={["click"]}>
                    <Button
                        shape="circle"
                        className={this.props.currentItemReducer.buttons[this.props.buttonIndex]}/>
                </Dropdown>
            </div>
        );
    }
}
const mapStateToProps = state => ({currentItemReducer: state.currentItemReducer});

const mapDispatchToProps = dispatch => ({
    actions: bindActionCreators({
        ...currentItemActions
    }, dispatch)
});

export default connect(mapStateToProps, mapDispatchToProps)(ItemButton);