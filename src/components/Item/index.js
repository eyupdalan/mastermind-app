import React, {Component} from 'react';
import {Button as AntdButton} from "antd";
import Button from "../Button";
import {connect} from "react-redux";
import {bindActionCreators} from "redux";
import * as currentItemActions from "../../redux/actions/currentItemActions";

export class Item extends Component {
    render() {
        return (
            <div className="mastermind-item">
                <div>
                    <Button buttonIndex={0}/>
                    <Button buttonIndex={1}/>
                    <Button buttonIndex={2}/>
                    <Button buttonIndex={3}/>
                </div>
                <div>
                    <AntdButton
                        type="primary"
                        style={{
                        margin: "5px"
                    }}>Check</AntdButton>
                </div>
                <div>
                    <AntdButton
                        style={{
                        margin: "5px"
                    }}
                        onClick={() => this.props.actions.resetCurrentButtonColor()}>Reset</AntdButton>
                </div>
            </div>
        );
    }
}

const mapDispatchToProps = dispatch => ({
    actions: bindActionCreators({
        ...currentItemActions
    }, dispatch)
});

export default connect(null, mapDispatchToProps)(Item);