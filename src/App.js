import "antd/dist/antd.css";
import './App.css';

import React, {Component} from 'react';
import {Provider} from "react-redux";
import configureStore from "./redux/configureStore";

import Item from "./components/Item";

class App extends Component {
  render() {
    return (
      <Provider store={configureStore()}>
        <div className="mastermind-app">
          <Item/>
        </div>
      </Provider>
    );
  }
}

export default App;
