import {combineReducers} from "redux";
import testReducer from "./reducers/testReducer";
import currentItemReducer from "./reducers/currentItemReducer";

export default combineReducers({testReducer, currentItemReducer});