import * as CurrentItemActionTypes from "../actions/currentItemActions";

const initialState = {
    buttons: ["", "", "", ""]
};

export default(state = initialState, action) => {
    switch (action.type) {
        case CurrentItemActionTypes.SET_CURRENT_BUTTON_COLOR:
            {
                let newState = Object.assign({}, state);
                newState.buttons[action.buttonIndex] = action.color;
                return newState;
            }
        case CurrentItemActionTypes.RESET_CURRENT_BUTTON_COLOR:
            {
                if (action.buttonIndex === null || action.buttonIndex === undefined || action.buttonIndex < -1) {
                    return {
                        buttons: ["", "", "", ""]
                    };
                }

                let newState = Object.assign({}, state);
                newState.buttons[action.buttonIndex] = "";
                return newState;
            }
        default:
            return state;
    }
}