export const TEST_ACTION = "TEST_ACTION";
export function testAction(payload) {
    return {type: TEST_ACTION, payload: payload}
}