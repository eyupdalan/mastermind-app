export const SET_CURRENT_BUTTON_COLOR = "SET_CURRENT_BUTTON_COLOR";
export const RESET_CURRENT_BUTTON_COLOR = "RESET_CURRENT_BUTTON_COLOR";

export function setCurrentButtonColor(buttonIndex, color) {
    return {type: SET_CURRENT_BUTTON_COLOR, buttonIndex, color};
}

export function resetCurrentButtonColor(buttonIndex, color) {
    return {type: RESET_CURRENT_BUTTON_COLOR, buttonIndex, color}
}